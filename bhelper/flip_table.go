package main

import "fmt"

func main() {
	t := flipTable()

	fmt.Println("var flipTable = [64]uint64{")
	for _, e := range t {
		fmt.Printf("\t%d,\n", e)
	}
	fmt.Println("}")

}

func flipTable() (table [64]uint64) {

	var (
		p = 0
		s = 2
	)

	for p < 63 {
		for row := 63; row >= 0; row-- {
			table[row] |= 1 << p
			if row%s == 0 {
				p++
			}
		}
		s *= 2
	}

	return
}
