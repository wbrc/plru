package plru

import (
	"math/rand"
	"sync"

	"github.com/cespare/xxhash"
)

type Cache struct {
	buckets   []*bucket
	positions map[uint64]uint64
	len, cap  uint64
	lock      *sync.Mutex
}

func New(cap uint64) *Cache {
	buckets := make([]*bucket, 32)
	for i := range buckets {
		buckets[i] = new(bucket)
	}
	return &Cache{
		buckets:   buckets,
		positions: make(map[uint64]uint64),
		cap:       cap,
		lock:      &sync.Mutex{},
	}
}

// test

func (c *Cache) evict() {
	for c.len > c.cap {
		bucketIndex := uint64(rand.Intn(len(c.buckets)))
		_, key, len := c.buckets[bucketIndex].put(cell{})
		if len > 0 {
			c.len -= len
			delete(c.positions, key)
		}
	}
}

func (c *Cache) Insert(key string, value []byte) {
	intKey := xxhash.Sum64String(key)

	c.Delete(key)

	c.lock.Lock()
	defer c.lock.Unlock()

	bucketIndex := uint64(rand.Intn(len(c.buckets)))

	cellIndex, oldKey, oldLen := c.buckets[bucketIndex].put(cell{intKey, value})

	if oldLen > 0 {
		delete(c.positions, oldKey)
		c.len -= oldLen
	}

	c.len += uint64(cap(value))
	c.positions[intKey] = mkPos(bucketIndex, cellIndex)

	c.evict()

	if len(c.positions) > len(c.buckets)*57 {
		c.buckets = append(c.buckets, new(bucket))
	}
}

func (c *Cache) Get(key string) ([]byte, bool) {
	intKey := xxhash.Sum64String(key)

	c.lock.Lock()
	defer c.lock.Unlock()

	if pos, ok := c.positions[intKey]; ok {
		bucketIndex, cellIndex := pToIdc(pos)
		c.buckets[bucketIndex].ref(cellIndex)
		return c.buckets[bucketIndex].elems[cellIndex].value, true
	}

	return nil, false
}

func (c *Cache) Delete(key string) {
	intKey := xxhash.Sum64String(key)

	c.lock.Lock()
	defer c.lock.Unlock()

	if pos, ok := c.positions[intKey]; ok {
		bucketIndex, cellIndex := pToIdc(pos)

		c.len -= uint64(cap(c.buckets[bucketIndex].elems[cellIndex].value))
		delete(c.positions, intKey)

		c.buckets[bucketIndex].clearCell(cellIndex)
	}
}

const (
	cellMask = (1 << 6) - 1
)

func mkPos(bIdx, cIdx uint64) uint64 {
	return (bIdx << 6) | cIdx
}

func pToIdc(pos uint64) (bIdx, cIdx uint64) {
	bIdx = pos >> 6
	cIdx = pos & cellMask
	return
}
